package com.example.user.examenb2018b;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_SCREEN_DELAY = 3000;
    private ProgressBar mprogressBar;
    private boolean mactive;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mprogressBar = (ProgressBar)findViewById(R.id.progressBarSplash);

        final Thread timerThread= new Thread(){
            @Override
            public void run() {
                mactive = true;
                try{
                    int carga=0;
                    while(mactive && (carga<SPLASH_SCREEN_DELAY)){
                        sleep(200);
                        if(mactive){
                            carga += 200;
                            actualizarProgressBar(carga);
                        }
                    }

                }catch (InterruptedException e){

                }finally {
                    Continuar();
                }
            }
        };
        timerThread.start();
    }
    private void Continuar() {
        Intent mainIntent = new Intent().setClass(SplashActivity.this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }

    private void actualizarProgressBar(final int time) {
        if(null != mprogressBar){
            final int progress= mprogressBar.getMax() * time/SPLASH_SCREEN_DELAY;
            mprogressBar.setProgress(progress);
        }
    }
}
