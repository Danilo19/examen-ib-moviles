package com.example.user.examenb2018b;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Usuario usuario;
    private CountDownTimer countDownTimer;
    private long time = 10000;
    private boolean banderaInicio;
    private Random random;
    private int valor1=50,valor2=100;
    private int numeroRandomico;
    private int puntajeObtenido;
    private TextView textViewPuntajeObtenido, textViewPuntajeTotal,textViewrandomico;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario = new Usuario("Invitado :D");
        banderaInicio = false;
        random = new Random();
        textViewPuntajeObtenido = (TextView) findViewById(R.id.textViewPuntaje);
        textViewPuntajeTotal = (TextView) findViewById(R.id.textViewTotal);
        textViewrandomico = (TextView)findViewById(R.id.textViewRandom);
    }

    public void Inciiar(View vew){
        banderaInicio = true;
        numeroRandomico = random.nextInt(10);
        textViewrandomico.setText(" "+numeroRandomico);
        time = 10000;
        puntajeObtenido =0;
        countDownTimer = new CountDownTimer(time,1000) {
            @Override
            public void onTick(long l) {
                time = l;
                if(l==0)
                    countDownTimer.cancel();
            }
            @Override
            public void onFinish() {

            }
        }.start();
    }

    public void Detener(View view){
        if(banderaInicio){
            banderaInicio = false;
            countDownTimer.cancel();
            int tiempoRestante = ((int) time)/1000;
            if(tiempoRestante==numeroRandomico)
                puntajeObtenido=valor2;
            else
                if(tiempoRestante-1==numeroRandomico || tiempoRestante+1==numeroRandomico)
                    puntajeObtenido = valor1;
                else
                    puntajeObtenido=0;
            textViewPuntajeObtenido.setText("Puntaje obtenido: "+puntajeObtenido);
            usuario.setPuntajeActual(usuario.getPuntajeActual()+puntajeObtenido);
            textViewPuntajeTotal.setText("Puntaje total "+usuario.getPuntajeActual());
        }else{
            Toast.makeText(getApplicationContext(),"No a iniciado el juego",Toast.LENGTH_SHORT);
        }
    }
}
