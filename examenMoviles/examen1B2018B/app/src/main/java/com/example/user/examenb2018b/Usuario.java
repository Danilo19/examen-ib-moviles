package com.example.user.examenb2018b;

public class Usuario {
    private String nombreusuario;
    private int puntajeActual;

    public Usuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
        this.puntajeActual = 0;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public int getPuntajeActual() {
        return puntajeActual;
    }

    public void setPuntajeActual(int puntajeActual) {
        this.puntajeActual = puntajeActual;
    }
}
